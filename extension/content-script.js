/* global chrome */

let port = chrome.runtime.connect({name: 'jira-toggle-tag-import'})

// Listen for messages from background process
port.onMessage.addListener(function (msg) {
	// If we're on the jira page do the thing we want to do when we click the button... and we're on the jira page
	if (window.location.href.includes('https://jira.creditsense.io/')) {
		handleJira()
	}
	else {
		throw new Error('Unexpected listener')
	}
})

// Handle message to trigger jira import
function handleJira () {
	console.log('Got request for jira stories!')

	const stories = []

	const storyWrappers = getStoryWrappers()
	for (let storyWrapper of storyWrappers) {
		stories.push({id: getStoryId(storyWrapper), name: getStoryName(storyWrapper)})
	}

	port.postMessage({stories})
}

function getStoryWrappers () {
	return document.querySelectorAll('.ghx-swimlane')
}

function getStoryId (storyWrapper) {
	return storyWrapper.querySelector('.ghx-swimlane-header').getAttribute('data-issue-key')
}

function getStoryName (storyWrapper) {
	return storyWrapper.querySelector('.ghx-summary').lastChild.textContent
}
