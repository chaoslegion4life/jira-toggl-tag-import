/* global chrome fetch */

// When the extension is installed or upgraded ...
chrome.runtime.onInstalled.addListener(function () {
// Replace all rules ...
	chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
		// With a new rule ...
		chrome.declarativeContent.onPageChanged.addRules([
			{
				conditions: [
					new chrome.declarativeContent.PageStateMatcher({
						pageUrl: { urlContains: 'https://jira.creditsense.io/' },
					}),
				],
				// And shows the extension's page action.
				actions: [ new chrome.declarativeContent.ShowPageAction() ],
			},
		])
	})
})

chrome.runtime.onConnect.addListener(function (port) {
	console.assert(port.name === 'jira-toggle-tag-import')

	chrome.pageAction.onClicked.addListener(function callback (tab) {
		console.log('Browser button clicked')
		port.postMessage({})
	})

	port.onMessage.addListener(function (msg) {
		if (msg.stories) {
			handleStories(msg.stories)
		}
		else {
			throw new Error('Unrecognised message from content script')
		}
	})
})

async function handleStories (stories) {
	const userRequest = await fetch('https://www.toggl.com/api/v8/me')
	if (userRequest.status === 403) {
		chrome.notifications.create({
			type: 'basic',
			title: 'Jira -> Toggle Tag Import',
			message: 'Error: Could not authenticate with Toggl. Ensure your browser is logged in.',
			iconUrl: 'icon.png',
		})
		return
	}

	const user = await userRequest.json()
	console.log(user)

	const workspaceId = user.data.default_wid

	const tagsRequest = await fetch(`https://www.toggl.com/api/v8/workspaces/${workspaceId}/tags`)
	const tags = await tagsRequest.json() || []
	console.log(tags)

	let tagsCreated = 0
	for (const story of stories) {
		const tagExistsForStory = !!tags.find(tag => tag.name === story.name)

		if (!tagExistsForStory) {
			const createTagRequest = await fetch(`https://www.toggl.com/api/v8/tags`, {
				method: 'POST',
				body: JSON.stringify({
					tag: {
						wid: workspaceId,
						name: story.id,
						// name: `${story.id} ${story.name}`,
					},
				}),
			})
			tagsCreated++
		}
	}

	chrome.notifications.create({
		type: 'basic',
		title: 'Jira -> Toggle Tag Import',
		message: `Success: Created ${tagsCreated} stories`,
		iconUrl: 'icon.png',
	})
}
